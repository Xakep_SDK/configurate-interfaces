package dk.xakeps.configurate.interfaces;

import ninja.leaping.configurate.objectmapping.ObjectMappingException;

public interface ImplementationGenerator {
    <T> T generate(Class<T> tClass) throws ObjectMappingException;
}

package dk.xakeps.configurate.interfaces.proxy;

import dk.xakeps.configurate.interfaces.Saver;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;

public class ProxyInvocationHandler implements InvocationHandler {
    private final Callable<Void> saveCallback;
    private final Map<String, Holder> holders;

    public ProxyInvocationHandler(Callable<Void> saveCallback, Map<String, Holder> holders) {
        this.saveCallback = saveCallback;
        this.holders = Objects.requireNonNull(holders, "holders");
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        switch (method.getName()) {
            case "hashCode": {
                return method.hashCode();
            }
            case "toString": {
                return holders.toString();
            }
            case "equals": {
                return proxy == args[0];
            }
        }

        Saver annotation = method.getAnnotation(Saver.class);
        if (annotation != null) {
            saveCallback.call();
            return null;
        }

        if (method.getReturnType() != void.class) {
            return holders.get(method.getName()).getObject();
        }
        holders.get(method.getName()).setObject(args[0]);
        return null;
    }

}

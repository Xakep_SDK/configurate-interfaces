package dk.xakeps.configurate.interfaces.proxy;

import com.google.common.reflect.TypeToken;
import dk.xakeps.configurate.interfaces.CustomSetting;
import dk.xakeps.configurate.interfaces.ImplementationGenerator;
import dk.xakeps.configurate.interfaces.Saver;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ProxyImplementationGenerator implements ImplementationGenerator {
    private final ConfigurationLoader<? extends ConfigurationNode> loader;
    private final ConfigurationNode rootNode;

    public ProxyImplementationGenerator(ConfigurationLoader<? extends ConfigurationNode> loader, ConfigurationNode rootNode) {
        this.loader = Objects.requireNonNull(loader, "loader");
        this.rootNode = Objects.requireNonNull(rootNode, "rootNode");
    }

    @Override
    public <T> T generate(Class<T> tClass) throws ObjectMappingException {
        return newProxy(tClass);
    }

    private InvocationHandler constructHandler(Class<?> clazz) throws ObjectMappingException {
        if (!clazz.isInterface()) {
            throw new IllegalArgumentException("Not an interface: " + clazz.getName());
        }
        ConfigSerializable classConfigAnn = clazz.getDeclaredAnnotation(ConfigSerializable.class);
        if (classConfigAnn == null) {
            throw new IllegalArgumentException("No ConfigSerializable annotation. Class=" + clazz.getName());
        }

        Map<String, Holder> holders = new HashMap<>();
        for (Method method : clazz.getMethods()) {
            if (method.getParameterCount() > 1) {
                throw constructError("Too many parameters", method);
            }
            if (method.getAnnotation(Saver.class) != null) {
                continue;
            }
            CustomSetting settingAnn = method.getAnnotation(CustomSetting.class);
            if (settingAnn == null) {
                throw constructError("No setting annotation!", method);
            }
            String configNodeId = settingAnn.value().isEmpty() ? method.getName() : settingAnn.value();
            TypeToken<?> objectType = extractType(method);
            ConfigSerializable methodConfigAnn = method.getReturnType().getDeclaredAnnotation(ConfigSerializable.class);
            Holder holder;
            ConfigurationNode node = rootNode.getNode(configNodeId);
            if (!method.getReturnType().isInterface() || methodConfigAnn == null) {
                holder = holders.computeIfAbsent(method.getName(), s -> new NodeHolder(node, objectType, configNodeId, method.getName()));
            } else {
                Object generate = generate(method.getReturnType());
                holder = holders.computeIfAbsent(method.getName(), s -> new ProxyHolder(node, objectType, configNodeId, method.getName(), generate));
            }

            if (method.getReturnType().isPrimitive() && method.getReturnType() != void.class && !method.isDefault()) {
                holder.setObject(Array.get(Array.newInstance(method.getReturnType(), 1), 0)); // get default primitive value
            }

            if (!settingAnn.comment().isEmpty()) {
                holder.setComment(settingAnn.comment());
            }

            if (method.getReturnType() != void.class && holder.getObject() == null && method.isDefault()) {
                Object instance = Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, (proxy, method1, args) -> {
                    Constructor<MethodHandles.Lookup> constructor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class);
                    constructor.setAccessible(true);
                    MethodHandles.Lookup lookup = constructor.newInstance(clazz);
                    return lookup.unreflectSpecial(method1, method1.getDeclaringClass())
                            .bindTo(proxy)
                            .invoke();
                });
                try {
                    holder.setObject(method.invoke(instance));
                } catch (Throwable e) {
                    throw new IllegalArgumentException("Can't get default value", e);
                }
            }
        }
        return new ProxyInvocationHandler(this::save, holders);
    }

    private <T> T newProxy(Class<T> clazz) throws ObjectMappingException {
        return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, constructHandler(clazz));
    }

    private Void save() throws IOException {
        loader.save(rootNode);
        return null;
    }

    private TypeToken<?> extractType(Method method) {
        if (method.getReturnType() == void.class) {
            return TypeToken.of(method.getGenericParameterTypes()[0]);
        } else {
            return TypeToken.of(method.getGenericReturnType());
        }
    }

    private static IllegalArgumentException constructError(String msg, Method method) {
        return new IllegalArgumentException(msg + ". Class: " + method.getDeclaringClass().getName() + ", Method: " + method.getName());
    }
}

package dk.xakeps.configurate.interfaces.proxy;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;

import java.util.Objects;

public class ProxyHolder extends AbstractHolder {
    private Object proxy;

    protected ProxyHolder(ConfigurationNode node, TypeToken<?> objectType, String nodeId, String methodName, Object proxy) {
        super(node, objectType, nodeId, methodName);
        this.proxy = Objects.requireNonNull(proxy, "proxy");
    }

    @Override
    public Object getObject() {
        return proxy;
    }

    @Override
    public void setObject(Object object) {
        throw new UnsupportedOperationException("Can't change proxied implementation");
    }

    @Override
    public String toString() {
        return String.valueOf(getObject());
    }
}

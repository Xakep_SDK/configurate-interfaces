package dk.xakeps.configurate.interfaces.proxy;

import ninja.leaping.configurate.objectmapping.ObjectMappingException;

import java.util.Optional;

public interface Holder {
    Object getObject() throws ObjectMappingException;
    void setObject(Object object) throws ObjectMappingException;

    void setComment(String comment);
    Optional<String> getComment();

    String getNodeId();
    String getMethodName();
}

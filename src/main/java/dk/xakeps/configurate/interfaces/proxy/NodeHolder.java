package dk.xakeps.configurate.interfaces.proxy;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;

class NodeHolder extends AbstractHolder {
    protected NodeHolder(ConfigurationNode node, TypeToken<?> objectType, String nodeId, String methodName) {
        super(node, objectType, nodeId, methodName);
    }

    @Override
    public Object getObject() throws ObjectMappingException {
        return getNode().getValue(getObjectType());
    }

    @Override
    public void setObject(Object object) throws ObjectMappingException {
        this.getNode().setValue((TypeToken<Object>) getObjectType(), object);
    }

    @Override
    public String toString() {
        try {
            return String.valueOf(getObject());
        } catch (ObjectMappingException e) {
            throw new RuntimeException("Can't get object", e);
        }
    }
}

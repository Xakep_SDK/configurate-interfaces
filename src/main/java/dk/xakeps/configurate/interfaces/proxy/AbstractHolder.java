package dk.xakeps.configurate.interfaces.proxy;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;

import java.util.Objects;
import java.util.Optional;

public abstract class AbstractHolder implements Holder {
    private final ConfigurationNode node;
    private final TypeToken<?> objectType;
    private final String nodeId;
    private final String methodName;

    protected AbstractHolder(ConfigurationNode node, TypeToken<?> objectType, String nodeId, String methodName) {
        this.node = Objects.requireNonNull(node, "node");
        this.objectType = Objects.requireNonNull(objectType, "objectType");
        this.nodeId = Objects.requireNonNull(nodeId, "nodeId");
        this.methodName = Objects.requireNonNull(methodName, "methodName");
    }

    @Override
    public void setComment(String comment) {
        if (node instanceof CommentedConfigurationNode) {
            ((CommentedConfigurationNode) node).setComment(comment);
        }
    }

    @Override
    public Optional<String> getComment() {
        if (node instanceof CommentedConfigurationNode) {
            return ((CommentedConfigurationNode) node).getComment();
        }
        return Optional.empty();
    }

    @Override
    public String getNodeId() {
        return nodeId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    protected ConfigurationNode getNode() {
        return node;
    }

    protected TypeToken<?> getObjectType() {
        return objectType;
    }
}

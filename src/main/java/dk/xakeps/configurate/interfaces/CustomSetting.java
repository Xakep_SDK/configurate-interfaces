package dk.xakeps.configurate.interfaces;


import ninja.leaping.configurate.objectmapping.ObjectMapper;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a field to be mapped by an {@link ObjectMapper}.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface CustomSetting {
    /**
     * The path this setting is located at
     *
     * @return The path
     */
    String value() default "";

    /**
     * The default comment associated with this configuration node
     * This will be applied to any comment-capable configuration loader
     *
     * @return The comment
     */
    String comment() default "";
}

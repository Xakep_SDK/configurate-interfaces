package dk.xakeps.configurate.interfaces;

import dk.xakeps.configurate.interfaces.proxy.ProxyImplementationGenerator;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Tests {
    public static void main(String[] args) throws IOException, ObjectMappingException {
        HoconConfigurationLoader build = HoconConfigurationLoader.builder()
                .setPath(Paths.get("test_config.hocon"))
                .build();
        CommentedConfigurationNode load = build.load();
        ProxyImplementationGenerator generator = new ProxyImplementationGenerator(build, load);

        TestConfig generate = generator.generate(TestConfig.class);

        System.out.println(generate.innerConfig().innerInt());

        System.out.println("Before change: " + generate);
        generate.test("newVal");
        System.out.println("After change: " + generate);

        generate.innerConfig().innerInt(30);
        generate.strings(Arrays.asList("first", "string"));

        System.out.println(generate.strings());
        generate.save();

        System.out.println(new String(Files.readAllBytes(Paths.get("test"))));
    }

    @ConfigSerializable
    public interface TestConfig {
        @CustomSetting(value = "customValue")
        default String test() {
            return "testString";
        }
        @CustomSetting(value = "customValue")
        void test(String val);

        @CustomSetting
        InnerConfig innerConfig();

        @CustomSetting
        List<String> strings();

        @CustomSetting
        void strings(List<String> strings);

        @Saver
        void save();
    }

    @ConfigSerializable
    public interface InnerConfig {
        @CustomSetting
        int innerInt();

        @CustomSetting
        void innerInt(int val);
    }
}
